#!/usr/bin/env bash

read -d '' AWK_SCRIPT << 'EOF'
{
  region=$1;
  if (region in last) {
    diff=$3-last[region];
  } else {
    diff=0;
  };

  last[region]=$3;
  print $1","$3","diff;
}
EOF

cat -     \
  | sort -n \
  | awk -F',' "${AWK_SCRIPT}"
