#!/usr/bin/env bash

jq -c '. as $parent | .regions + {"total": $parent.total} | to_entries[] | "\(.key),\($parent.timestamp),\(.value.infections),\(.value.deaths)"' "$@" \
  | tr -d "\"" \
  | sort -n
