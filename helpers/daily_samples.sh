#!/usr/bin/env bash

if [[ $# -lt 1 ]]; then
  echo "Usage: $0 DATA_DIRECTORY" >&2
  exit 1
fi

ls "$1"/*.json                                                              \
  | sort -n                                                                 \
  | awk -F'_' '{ last[$2]=$0; } END { for (k in last) { print last[k]; } }' \
  | sort -n
