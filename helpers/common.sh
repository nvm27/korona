function cleanup {
  local tmp_file="$1"

  rm "${tmp_file}"
  echo "# Removed temporary data file: ${tmp_file}" >&2
}

function make_tmp_file {
  local tmp_file="$(mktemp)"

  echo "# Created temporary data file: ${tmp_file}" >&2
  echo "${tmp_file}"
}

function filter {
  if [[ $# -eq 0 ]]; then
    grep total
  else
    grep "$@"
  fi
}

