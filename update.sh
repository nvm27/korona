#!/usr/bin/env bash

script_name="$0"

function date_fmt {
  date -Iseconds
}

function err_report {
  local msg="Error occured while updating 'korona' repository: ${script_name} on line $1."
  echo "$(date_fmt) ${msg}" >&2

  if command -v notify.sh >/dev/null; then
    notify.sh ERR "${msg}"
  fi

  exit 1
}

trap 'err_report $LINENO' ERR

cd "$(dirname "$0")"

./download_data.sh &>/dev/null
if ! git status --porcelain=v1 --untracked-files=all data | grep '??' >/dev/null; then
  exit 0
fi

echo "$(date_fmt) Updating 'korona' repository"

changes_in_workdir=$(git status --porcelain=v1 --untracked-files=no | wc -l)
[[ ${changes_in_workdir} -gt 0 ]] && git stash push

old_branch=$(git branch --show-current)
[[ "${old_branch}" != "master" ]] && git checkout master

git pull --ff-only
git add data
git commit --author "Update Bot <bot@bot.bot>" --message "Update infections data"
git push

[[ "${old_branch}" != "master" ]] && git checkout "${old_branch}"
[[ ${changes_in_workdir} -gt 0 ]] && git stash pop
