#!/usr/bin/env bash

cd "$(dirname "$0")"
source ./helpers/common.sh

DATA_FILE="$(make_tmp_file)"
trap "cleanup \"${DATA_FILE}\"" EXIT

./helpers/daily_samples.sh "data"  \
  | xargs ./helpers/json_to_csv.sh \
  | filter "$@"                    \
  | ./helpers/csv_cases_delta.sh   \
  | ./gnuplot/prepare.awk          \
  > "${DATA_FILE}"

./gnuplot/delta.gp "${DATA_FILE}"
