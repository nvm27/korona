#!/usr/bin/env bash

cd "$(dirname "$0")"
source ./helpers/common.sh

DATA_FILE="$(make_tmp_file)"
trap "cleanup \"${DATA_FILE}\"" EXIT

./helpers/json_to_csv.sh data/* \
  | filter "$@"                 \
  | ./gnuplot/prepare.awk       \
  > "${DATA_FILE}"

./gnuplot/cases.gp "${DATA_FILE}"
