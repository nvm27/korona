#!/usr/bin/env -S gnuplot --persist -c

datafile=ARG1

set title 'Trajectory of COVID-19 Confirmed Cases'
set xlabel 'Total Confirmed Cases'
set ylabel 'New Confirmed Cases'

set logscale
set logscale y

stats datafile skip 1

plot                         \
  for [IDX=0:STATS_blocks-1] \
  datafile                   \
  index IDX                  \
  using 1:2                  \
  with linespoint pt 7       \
  title columnheader(1)
