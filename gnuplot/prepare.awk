#!/usr/bin/env -S awk -F',' -f

# Translates input in format:
# region1,a,b
# region2,x,y
# ...
# into gnuplot input with regions as separate data sets

{
  entry=$2" "$3;

  if ($1 in regions) {
    regions[$1]=regions[$1]"\n"entry;
  } else {
    regions[$1]=entry;
  }
}

END {
  first=1;
  for (region in regions) {
    if (first == 1) { first=0; } else { print ""; }
    print "\""region"\"";
    print regions[region]"\n";
  }
}
