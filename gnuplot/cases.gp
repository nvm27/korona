#!/usr/bin/env -S gnuplot --persist -c

datafile=ARG1

set title 'COVID-19 Confirmed Cases'
set xlabel 'Time'
set ylabel 'Total Confirmed Cases'

stats datafile skip 1

set xdata time
set timefmt "%s"
set format x "%d/%m"

plot                         \
  for [IDX=0:STATS_blocks-1] \
  datafile                   \
  index IDX                  \
  using 1:2                  \
  with lines                 \
  title columnheader(1)
