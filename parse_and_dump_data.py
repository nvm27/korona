#!/usr/bin/env python3

import json
import sys
import re
import os

from datetime import datetime

path_prefix = "data/delta/"

def cd_to_script_directory():
    directory = os.path.dirname(sys.argv[0])
    if directory:
        os.chdir(directory)

def determine_file_path(timestamp: datetime) -> str:
    return timestamp.strftime(path_prefix + "korona_%Y-%m-%d_%H%M.json")

def normalize(input_str: str):
    return input_str.lower().translate(str.maketrans("ąćęłńóśźż", "acelnoszz"))

def import_int(value):
    return int(value.replace(" ", ""))

def import_regions(region_list):
    regions = dict()

    for entry in region_list:
        (region, infections, deaths) = [entry[key] for key in ("Województwo", "Liczba", "Wszystkie przypadki śmiertelne")]
        region = normalize(region)

        regions[region] = dict()
        regions[region]["infections"] = import_int(infections) if infections else 0
        regions[region]["deaths"] = import_int(deaths) if deaths else 0

    return regions

def dump_data_to_file(file_path: str, document):
    with open(file_path, "w") as file:
        json.dump(document, file, sort_keys=True, indent=2)
        file.write("\n")

def import_data(region_list, time_point):
    document = dict()

    document["time"] = time_point.isoformat()
    document["timestamp"] = int(time_point.timestamp())
    document["regions"] = import_regions(region_list)
    document["total"] = document["regions"].pop(normalize("Cały Kraj"))

    return document

if __name__ == "__main__":
    cd_to_script_directory()

    raw_data = json.load(sys.stdin)

    time_point = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
    file_path = determine_file_path(time_point)

    if os.path.isfile(file_path):
        print(f"File {file_path} exists. Skipping...")
        sys.exit(0)

    document = import_data(json.loads(raw_data["parsedData"]), time_point)

    dump_data_to_file(file_path, document)

    print("File created:", file_path)
