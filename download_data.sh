#!/usr/bin/env bash

cd "$(dirname "$0")"

curl -XGET https://www.gov.pl/web/koronawirus/wykaz-zarazen-koronawirusem-sars-cov-2  \
  | grep -F 'id="registerData"' \
  | grep -oh '{.*}' \
  | sed 's/\\&quot;/\\"/g' \
  | sed 's/&oacute;/ó/g' \
  | sed 's/&#347;/ś/g' \
  | sed 's/&#322;/ł/g' \
  | sed 's/&#281;/ę/g' \
  | sed 's/&#324;/ń/g' \
  | sed 's/&#261;/ą/g' \
  | /usr/bin/env python3 parse_and_dump_data.py
